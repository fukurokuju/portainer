#!/usr/bin/env bash

if [[ -z "$1" ]]; then
  echo "no project name was provided"
  exit 1
fi

PROJECT=$1

set -euo pipefail

git-cliff -c "$PROJECT/cliff.toml"| uniq > "$PROJECT/CHANGELOG"
