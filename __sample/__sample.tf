terraform {
  backend "s3" {
    bucket = "fuku-terraform-state-__sample-authentik"
    key    = "terraform.tfstate"
    region = "eu-west-3"
  }

  required_providers {
    authentik = {
      source  = "goauthentik/authentik"
      version = "2022.12.0"
    }
  }
}

resource "authentik_flow" "default_flow" {
  slug               = "default-provider-authorization-implicit-consent"
  title              = "Redirecting to %(app)s"
  name               = "Authorize Application"
  designation        = "authorization"
  policy_engine_mode = "all"
}

resource "authentik_provider_oauth2" "__sample" {
  name               = "__sample"
  client_id          = var.oidc_client_id
  client_secret      = var.oidc_client_secret
  authorization_flow = authentik_flow.default_flow.uuid
  property_mappings  = var.oidc_property_mappings
  redirect_uris      = var.oidc_redirect_uris
  signing_key        = var.oidc_signing_key
}

resource "authentik_application" "__sample" {
  name              = "__sample"
  slug              = "__sample"
  meta_icon         = "https://__sample.app/favicon.ico"
  meta_description   = ""
  meta_launch_url   = "https://__sample.roboces.dev"
  open_in_new_tab   = true
  protocol_provider = authentik_provider_oauth2.__sample.id
}