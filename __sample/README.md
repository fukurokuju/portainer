# __sample service

## Install

```text
$ cp sample.env .env
$ $EDITOR .env
$ source .env
$ terraform apply
$ docker compose up -d
```
