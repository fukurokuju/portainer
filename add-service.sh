#!/usr/bin/env bash
if [[ -z "$1" ]]; then
  echo "no project name was provided"
  exit 1
fi

set -euo pipefail

PROJECT=$1

if [[ -d "$PROJECT" ]]; then
  echo "project $PROJECT already exists"
  exit 2
fi

cp -r __sample "$PROJECT"
grep -rl '__sample' "$PROJECT" | xargs sed -i "s/__sample/$PROJECT/g"
mv "$PROJECT/__sample.tf" "$PROJECT/$PROJECT.tf"
