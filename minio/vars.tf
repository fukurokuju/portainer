variable "oidc_client_id" {
  type        = string
  description = "Authentik Client ID"
}

variable "oidc_property_mappings" {
  type        = list(string)
  description = "Property mappings"
  default     = [
    "c91e5460-6f34-4fa3-8364-1384bc108ed1",
    "b6ce2fd2-b79d-412e-b113-5881eb996fcc",
    "246d897f-9cdc-4011-be12-dfb296c92865",
    "d14ebe91-c675-4a53-a33e-d8fd8adfc3fc"
  ]
}

variable "oidc_redirect_uris" {
  type        = list(string)
  description = "List of Redirect URI"
  default     = [
    "https://console.s3.fukurokuju.dev/oauth_callback"
  ]
}

variable "oidc_signing_key" {
  type        = string
  description = "Signing key"
  default     = "c4ff5edf-3cad-4093-9326-44fea088e670"
}
