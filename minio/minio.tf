terraform {
  backend "s3" {
    bucket = "fuku-terraform-state-minio-authentik"
    key    = "terraform.tfstate"
    region = "eu-west-3"
  }

  required_providers {
    authentik = {
      source  = "goauthentik/authentik"
      version = "2022.12.0"
    }
  }
}

resource "authentik_flow" "default_flow" {
  slug               = "default-provider-authorization-implicit-consent"
  title              = "Redirecting to %(app)s"
  name               = "Authorize Application"
  designation        = "authorization"
  policy_engine_mode = "all"
}

resource "authentik_provider_oauth2" "minio" {
  name               = "minio"
  client_id          = var.oidc_client_id
  client_type        = "public"
  authorization_flow = authentik_flow.default_flow.uuid
  property_mappings  = var.oidc_property_mappings
  redirect_uris      = var.oidc_redirect_uris
  signing_key        = var.oidc_signing_key
}

resource "authentik_application" "minio" {
  name              = "Minio"
  slug              = "minio"
  meta_icon         = "https://repository-images.githubusercontent.com/29261473/ff41a900-6bfa-11e9-95fe-aa27fe8b337b"
  meta_description   = "S3 storage"
  meta_launch_url   = "https://console.s3.fukurokuju.dev"
  open_in_new_tab   = true
  protocol_provider = authentik_provider_oauth2.minio.id
}