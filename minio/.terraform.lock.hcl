# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/goauthentik/authentik" {
  version     = "2022.12.0"
  constraints = "2022.12.0"
  hashes = [
    "h1:UU2uR1nHsPwGNhvgGZbHN2QsUoOpyqj6hbeWY4kinMM=",
    "zh:130d4bd8754326e7a7a669f40ecfc0670de77ea8184c660e32c253e537f0f77f",
    "zh:1b43be5c78e66671466862df4fd1c2a1d4ade8a549a34359de0e9fc9c44caad7",
    "zh:2a15fc8529d457a2333f8ed16e7a61cb1a5cde2f2526a5d89330566072cfab25",
    "zh:3c515e2d8ba2b52597798e4e9020d8aebe023347f849c6f83e3b952f9e9084fa",
    "zh:3eb23674f4e0209e9ff375a6e0df8c47d407d84926cbe5ff808541244e5af4b1",
    "zh:46a0c2125ce074eb7c5108aabe1adcb629c1e1a398e781c313d679e6b3605338",
    "zh:4b1954469ba0b58c772b5ecd8cf3fddc79da63b5a8d94549dfceef45db29d32a",
    "zh:4bdb01f3114d93278c441b2d42ab2cd814089700d96900001b05497001266714",
    "zh:711cbad3ee046514f5135b83ebe1ffaafe2607a5f8164be034232acb120752c7",
    "zh:761b469cb2ff98730d50b6dde4aab13501fde2ef058b16bbbff69781fd933a39",
    "zh:89449759a18f4bffb2304e9dc40afa1ac57253a570b81ea1cc7b512e87dc1398",
    "zh:a3f36a6aa8ee975a4530f3486ee3f0b5a458e592166551509e6069a5f98fa034",
    "zh:bbf06549ffbb86ad2e995006643c7c604c33f66eefa81299eb7693627d38fa3d",
    "zh:d868ac5bc782006e0407c46d00695e9f2beda7afc53025ce756c27642f3fae65",
  ]
}
