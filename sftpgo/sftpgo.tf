terraform {
  backend "s3" {
    bucket = "fuku-terraform"
    key    = "sftpgo/terraform.tfstate"
    region = "us-east-1"
  }

  required_providers {
    authentik = {
      source  = "goauthentik/authentik"
      version = "2023.6.0"
    }
  }
}

resource "authentik_flow" "default_flow" {
  slug               = "default-provider-authorization-implicit-consent"
  title              = "Redirecting to %(app)s"
  name               = "Authorize Application"
  designation        = "authorization"
  policy_engine_mode = "all"
}

resource "authentik_provider_oauth2" "sftpgo" {
  name               = "SFTPGo"
  client_id          = var.oidc_client_id
  client_secret      = var.oidc_client_secret
  client_type        = "public"
  authorization_flow = authentik_flow.default_flow.uuid
  property_mappings  = var.oidc_property_mappings
  redirect_uris      = var.oidc_redirect_uris
  signing_key        = var.oidc_signing_key
}

resource "authentik_application" "sftpgo" {
  name              = "SFTPGo"
  slug              = "SFTPGo"
  meta_icon         = "https://raw.githubusercontent.com/drakkan/sftpgo/main/img/logo.png"
  meta_description  = "SFTP"
  meta_launch_url   = "https://ftp.fukurokuju.dev"
  open_in_new_tab   = true
  protocol_provider = authentik_provider_oauth2.sftpgo.id
}