# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/goauthentik/authentik" {
  version     = "2023.6.0"
  constraints = "2023.6.0"
  hashes = [
    "h1:k6lj7yZrnEwJk6E+D/Y2w7Ig7ci4TbRAHClmEF6aw78=",
    "zh:1aa4af55c4dcfb88608fe9dc0639f2227e6bff45ddc31af34b4b9545d089ce17",
    "zh:24d39dd8515d217c343f08ef4e4f9aeaa01daf1749be871f646a703e7583d559",
    "zh:3175887e7c87d38ecb93caae567486e77d736f65631f3915cff6c80482dba892",
    "zh:343cfc5e2bed337d53dc81c783d96435e85443aeaa0ad9eaaaee591e1428f143",
    "zh:35a21b0cce199d85ce9d3287b2a3de853a8de0d95ad96157f56140e37106b483",
    "zh:645ee03f1768989aa35a9ad69a0597eb04f6d7be9d154f33ac7702e557eff2bd",
    "zh:64f6152da0cb8effa2ea07b467c91c285735fb4591219ae949cbe92cc5d55210",
    "zh:95e72ab461980f58170941e64b74daa84db940b48e9cdb8ff6fd8b2a7b88a580",
    "zh:9a131deb8df2e171034308cbfdcce23454bce8e99ac925817a74c2873a56ca32",
    "zh:ad19e2de4f69d7122d019e0cfde9ef2e84c4baa988b77e1bef5e24bc30330c58",
    "zh:b0f366509dda43a42e0f66d792a3b2476cc7c61fc1bd544496e51d6f9b6517c4",
    "zh:c5a69f0415e4198497557d39f78f73361ae8b3a127ba5ed9521c4d89e0f88093",
    "zh:c9ebf630667ed427c1022b42a39dc5284a2f55626ebdc44b89f4ec79fbf19f4a",
    "zh:e84a91474c5f32b1c6fdabf770fa677bb84327fbe6107e49f27097e03cc2018c",
  ]
}
